import { MutationTree } from "vuex";
import { State, Post, Comment } from "./state";

export enum MutationType {
  SetPosts = "Set_Posts",
  SetPostById = "Set_Post_By_Id",
  SetPostComments = "Set_Post_Comments",
}

export type Mutations = {
  [MutationType.SetPosts](state: State, post: Post[]): void;
  [MutationType.SetPostById](state: State, post: Post[]): void;
  [MutationType.SetPostComments](state: State, Comment: Comment[]): void;
};

export const mutations: MutationTree<State> & Mutations = {
  [MutationType.SetPosts](state, posts) {
    state.posts = posts;
  },
  [MutationType.SetPostById](state, posts) {
    state.currentPost = posts;
  },
  [MutationType.SetPostComments](state, comments) {
    state.currentPostComments = comments;
  },
};
