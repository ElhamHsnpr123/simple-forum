import axios from "axios";
import { ActionContext, ActionTree } from "vuex";
import { Mutations, MutationType } from "./mutation";
import { State } from "./state";

export enum ActionType {
  GetPost = "Get_Post",
  GetPostById = "Get_Post_By_Id",
  GetPostComments = "Get_Post_Comments",
}

type ActionAugments = Omit<ActionContext<State, State>, "commit"> & {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>;
};

export type Actions = {
  [ActionType.GetPost](context: ActionAugments): void;
  [ActionType.GetPostById](context: ActionAugments, id: number): void;
  [ActionType.GetPostComments](context: ActionAugments, id: number): void;
};

export const actions: ActionTree<State, State> & Actions = {
  async [ActionType.GetPost]({ commit }) {
    axios.get("https://jsonplaceholder.typicode.com/posts").then((response) => {
      commit(MutationType.SetPosts, response.data);
      // console.log("RESPONSE:", response.data);
    });
  },
  async [ActionType.GetPostById]({ commit }, id: number) {
    axios
      .get("https://jsonplaceholder.typicode.com/posts/" + id)
      .then((response) => {
        // console.log(response.data);
        commit(MutationType.SetPostById, response.data);
      });
  },
  async [ActionType.GetPostComments]({ commit }, id: number) {
    axios
      .get("https://jsonplaceholder.typicode.com/posts/" + id + "/comments")
      .then((response) => {
        console.log("comments", response.data);
        commit(MutationType.SetPostComments, response.data);
      });
  },
};
