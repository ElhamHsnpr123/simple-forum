export interface Post {
  id: number;
  title: string;
  description: string;
  userId: number;
}

export interface Comment {
  id: number;
  name: string;
  email: string;
  description: string;
}

export interface State {
  posts: Post[];
  currentPostComments: Comment[];
  currentPost: Post[];
}

export const state: State = {
  posts: [],
  currentPostComments: [],
  currentPost: [],
};
