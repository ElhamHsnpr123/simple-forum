import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import PostList from "../components/PostList.vue";
import PostDetails from "../components/PostDetails.vue";
import PostCreate from "../components/PostCreate.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "PostList",
    component: PostList,
  },
  {
    path: "/post/:id",
    name: "PostDetails",
    component: PostDetails,
  },
  {
    path: "/create",
    name: "PostCreate",
    component: PostCreate,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
