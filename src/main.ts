import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { store } from "./store";
import "ant-design-vue/dist/antd.css";
import {
  List,
  Pagination,
  Button,
  Form,
  Input,
  Spin,
  Alert,
} from "ant-design-vue";

createApp(App)
  .use(store)
  .use(router)
  .use(List)
  .use(Pagination)
  .use(Button)
  .use(Form)
  .use(Input)
  .use(Spin)
  .use(Alert)
  .mount("#app");
